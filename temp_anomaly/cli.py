import argparse
import datetime
from pathlib import Path
import os

from . import calc, io, plot


def temp_anomaly():
    """Entry point for the minimal example"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input-path",
        dest="input_path",
        required=True,
        help="Full path of input data.",
    )
    parser.add_argument(
        "--start-date",
        dest="start_date",
        required=False,
        default="1990-01",
        type=lambda s: datetime.datetime.strptime(s, "%Y-%m"),
        help="Start date in format YYYY-MM (default = 1990-01)",
    )
    parser.add_argument(
        "--end-date",
        dest="end_date",
        required=False,
        default="2021-12",
        type=lambda s: datetime.datetime.strptime(s, "%Y-%m"),
        help="End date in format YYYY-MM (default = 2021-12)",
    )
    parser.add_argument(
        "--lat-range",
        dest="lat_range",
        required=False,
        default=(-90, 90),
        type=float,
        help="Select latitude range (default = -90° to 90°)",
        nargs=2,
        metavar=("lat_min", "lat_max"),
    )
    parser.add_argument(
        "--anomalies",
        dest="anomalies",
        action="store_true",
        help="Compute and plot temperature anomalies.",
    )
    parser.add_argument(
        "--trend",
        dest="trend",
        action="store_true",
        help="Compute and plot a linear temperature trend.",
    )
    parser.add_argument(
        "--output-path",
        dest="output_path",
        required=False,
        help="Stores data to desired directory, filename needs to be provided.",
    )

    args = parser.parse_args()

    if not Path(args.input_path).exists():
        parser.error("Data file does not exist.")

    if args.output_path and not Path(args.output_path).parents[0].exists():
        parser.error("Output path does not exist.")

    name, extension = os.path.splitext(args.input_path)
    if extension == ".csv":
        ds = io.load_csv(args.input_path)

    elif extension == ".nc":
        ds = io.load_nc(args.input_path, args.lat_range)

    else:
        parser.error("Input file has the wrong format, needs to end with .nc or .csv.")

    if args.anomalies:
        ds = calc.calc_anomaly(ds, args.start_date, args.end_date)
        title = f"Temperature anomalies \n from {args.start_date:%Y-%m} to {args.end_date:%Y-%m}"
    else:
        title = f"Temperature time series at Graz University \n from {args.start_date:%Y-%m} to {args.end_date:%Y-%m}"

    if args.trend:
        ds = calc.calc_trend(ds, args.start_date, args.end_date)
        title = title + " + Trend"

    plot.plot_timeseries(
        ds, args.start_date, args.end_date, title, args.output_path, args.trend
    )

    if args.output_path:
        io.save_file(ds, args.output_path)
