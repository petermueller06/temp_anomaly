import matplotlib.pyplot as plt
import os


def plot_timeseries(ds, begin, end, title, path="", trend=""):
    """Function to plot a timeseries

    Args:
        ds: xarray dataset
        begin: desired date to begin
        end: desired date to end
        title: plot title (depending on whether anomalies or simple timeseries is plotted)
        path: desired directory to save the plot figure (optional)
        trend: if trend is given, a linear trend of the timeseries is plotted (optional)
    """
    print("plotting timeseries")
    ds = ds.sel(time=slice(begin, end))
    fig, ax = plt.subplots(figsize=(14,8))
    ax.scatter(ds.time, ds["t"], label="Timeseries", edgecolors='k', c=ds["t"], cmap='RdYlBu_r')
    if trend:
        ax.plot(ds.time, ds["trend"], label="Trend", color='crimson', linestyle='dashed', linewidth=3)
    # ax.set(title=title, xlabel="time (yr)", ylabel="temperature at 2 m (K)")
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.xlabel("time (yr)", fontsize=16)
    plt.ylabel("temperature at 2 m (K)", fontsize=16)
    plt.title(title, fontsize=16)
    ax.legend(fontsize=14)
    ax.grid()
    plt.show()

    if path:
        print(f"figures saved to {path}")
        head, tail = os.path.split(path)
        fig.savefig(os.path.join(head, "timeseries.png"))
