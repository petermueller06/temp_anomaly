import numpy as np


def calc_anomaly(ds, begin, end):
    """Calculates the temperature anomalies for a given timespan compared to the long term mean (1991-2020)

    Args:
        ds: xarray dataset
        begin: desired start date
        end: desired end date
    Returns:
        ds: xarray dataset
    """
    print("computing temperature anomalies")
    clim = ds.sel(time=slice("1991-01", "2020-12")).groupby("time.month").mean()
    ds = ds.sel(time=slice(begin, end))
    anom = ds.groupby("time.month") - clim

    return anom


def calc_trend(ds, begin, end):
    """Calculates trend over the given data.

    Args:
        ds: xarray dataset
        begin: desired start date
        end: desired end date
    Returns:
        ds: xarray dataset
    """
    print("computing trend")
    ds = ds.sel(time=slice(begin, end))

    time = np.arange(ds["t"].dropna("time").shape[0])
    temp = ds["t"].dropna("time").values

    coef = np.polyfit(time, temp, 1)
    time_fit = np.arange(ds["t"].shape[0])
    temp_fit_func = np.poly1d(coef)
    temp_fit = temp_fit_func(time_fit)

    ds = ds.assign({"trend": ds["t"] * 0 + temp_fit})

    return ds
