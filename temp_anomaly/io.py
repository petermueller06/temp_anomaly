import pandas as pd
import xarray as xr


def load_csv(path):
    """Reads data from given filepath, assuming netCDF format.

    Args:
        path: full path file of the data

    Returns:
        ds: xarray dataset
    """
    print("Reading csv data")
    df = pd.read_csv(path, parse_dates=["time"], index_col="time")
    ds = df.to_xarray()

    # change temperature data from °C to K
    ds["t"].data = ds["t"].data + 273.15
    ds["tmin"].data = ds["tmin"].data + 273.15
    ds["tmax"].data = ds["tmax"].data + 273.15
    ds["t"].attrs["units"] = "K"
    ds["tmin"].attrs["units"] = "K"
    ds["tmax"].attrs["units"] = "K"

    return ds


def load_nc(path, lat_range):
    """Reads data from given filepath, assuming netCDF format.

    Args:
        path: full path file of the data
        lat_range: desired latitude range

    Returns:
        ds: xarray dataset
    """

    print("reading netCDF data")
    with xr.open_dataset(path) as ds:
        ds.load()

    ds = ds.rename_vars({"temperature_2_meter": "t"})
    ds = ds.sel(latitude=slice(*lat_range)).mean(["longitude", "latitude"])

    return ds


def save_file(ds, path):
    """Saves data to desired directory
    Args:
        ds: xarray dataset
        path: desired directory path
    """
    print(f"saving to {path}")
    ds.to_netcdf(path)
