# temp_anomaly

A simple program to either plot a temperature timeseries or to
calculate the monthly temperature anomalies for a given timespan.

It contains:

1. A command line interface
2. An installable shell script

It provides the following shell script: `temp_anomaly`.
For usage information, type `temp_anomaly --help`.

## Installation

Use the following command in the base directory to install:

```bash
python -m pip install .
```

For an editable ("developer mode") installation, use the following
instead:

```bash
python -m pip install -e .
```

With this, the installation is actually a link to the original source code,
i.e. each change in the source code is immediately available.


## Prerequisites

You need a working Python environment, and `pip` installed.
It is recommended to create a virtual environment and to install the program there.

E.g., with `conda`:

```bash
conda create --name mynewenv python
conda activate mynewenv
python -m pip install -e .
```
or any other virtualenv wrapper if you are using Linux.

## Notes
The selected time frame for the "long-term mean", to calculate the monthly anomalies, starts at 1991-01 and ends 2020-12.
This was a random selection by the developer.


## Example
The following example shows how using the program could look like:
```bash
temp_anomaly --input-path /home/peter/documents/ECMWF.nc --lat-range 40 50 --start-date 2010-12 --end-date 2020-12 --anomalies --trend --output-path /home/peter/Pictures/test.nc 
```

in this case 
```bash
--anomalies
```
and 
```bash
--trend
```

are given, which means that monthly temperature anomalies and a trend are computed and plotted.